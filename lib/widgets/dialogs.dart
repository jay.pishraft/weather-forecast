import 'dart:ui';

import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:math' as math;


class AlertDialogWithDynamicActionButton extends StatelessWidget {
  final bool isTwoButton;
  final bool isNoButton;
  final bool isConfirmationDialog;
  final bool isTopTitle;
  final bool isError;
  final bool isErrorTitle;
  final String title;
  final Color titleColor;
  final Color titleBackgroundColor;
  final String message;
  final String rightButtonText;
  final String leftButtonText;
  final Function rightButtonAction;
  final Function leftButtonAction;
  final Widget bodyWidget;

  const AlertDialogWithDynamicActionButton(
      {Key key,
      this.rightButtonAction,
      this.leftButtonAction,
      this.bodyWidget,
      this.message,
      this.rightButtonText,
      this.leftButtonText,
      this.isError,
      this.title,
      this.isTopTitle,
      this.titleColor,
      this.titleBackgroundColor,
      this.isTwoButton,
      this.isErrorTitle,
      this.isConfirmationDialog,
      this.isNoButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      contentPadding: EdgeInsets.all(0.0),
      content: Container(
        padding: const EdgeInsets.all(8.0),
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            gradient: new LinearGradient(
                colors: [Colors.blue[900], Colors.blue[100]],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter)),
        child: Stack(
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: isError
                      ? Column(
                          children: <Widget>[
                            CircleAvatar(
                              radius: 30,
                              backgroundColor: Colors.white,
                              child: Icon(
                                Icons.error_outline,
                                color: Colors.pink,
                                size: 50,
                              ),
                            ),
                            isErrorTitle != null && isErrorTitle
                                ? isTopTitle && title != null
                                    ? Container(
                                        margin: EdgeInsets.only(top: 10.0),
                                        decoration: BoxDecoration(
                                            color: titleBackgroundColor,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(20))),
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10.0, vertical: 5),
                                        child: Text(
                                          title,
                                          style: TextStyle(
                                              color: titleColor, fontSize: 18),
                                        ),
                                      )
                                    : Container()
                                : Container()
                          ],
                        )
                      : Column(
                          children: <Widget>[
                            CircleAvatar(
                              radius: 28,
                              backgroundColor: Colors.white,
                              child: Image.asset(
                                "assets/images/weather03-512.png",
                                height: 40,
                                width: 40,
                              ),
                            ),
                            isTopTitle && title != null
                                ? Container(
                                    margin: EdgeInsets.only(top: 10.0),
                                    decoration: BoxDecoration(
                                        color: titleBackgroundColor,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20))),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10.0, vertical: 5),
                                    child: Text(
                                      title,
                                      style: TextStyle(
                                          color: titleColor, fontSize: 18),
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: isError && message != null
                            ? Row(
                                children: <Widget>[
                                  Expanded(
                                      child: Container(
                                          padding: EdgeInsets.all(10),
                                          child: Text(
                                            message,
                                            style: TextStyle(
                                                color: Colors.blue.shade800,
                                                fontSize: 14),
                                            textAlign: TextAlign.center,
                                          ))),
                                ],
                              )
                            : isConfirmationDialog == false && message != null
                                ? Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        child: bodyWidget,
                                      )),
                                    ],
                                  )
                                : isConfirmationDialog == true &&
                                            message == null ||
                                        message.isEmpty
                                    ? Row(
                                        children: <Widget>[
                                          Expanded(
                                              child: ClipRRect(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(20)),
                                            child: bodyWidget,
                                          )),
                                        ],
                                      )
                                    : Row(
                                        children: <Widget>[
                                          Expanded(
                                              child: Container(
                                                  padding: EdgeInsets.all(10),
                                                  child: Text(
                                                    message,
                                                    style: TextStyle(
                                                        color: Colors
                                                            .blue.shade800,
                                                        fontSize: 14),
                                                    textAlign: TextAlign.center,
                                                  ))),
                                        ],
                                      )),
                    isTopTitle || isError
                        ? Container()
                        : Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Text(
                              title,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 18),
                            ),
                          )
                  ],
                ),
                isNoButton
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(20.0)),
                          child: Container(
                            color: Colors.black.withOpacity(0.1),
                            child: Row(
                              children: <Widget>[
                                isError || isTwoButton == false
                                    ? Container()
                                    : Expanded(
                                        child: Container(
                                          child: FlatButton(
                                            onPressed: () => leftButtonAction(),
                                            child: Text(leftButtonText,
                                                style: TextStyle(
                                                    color: Colors.white)),
                                            color:
                                                Colors.black.withOpacity(0.1),
                                            padding: EdgeInsets.all(15),
                                          ),
                                        ),
                                      ),
                                isError || isTwoButton == false
                                    ? Container()
                                    : Container(
                                        color: Colors.white,
                                        height: 20,
                                        width: 0.5,
                                      ),
                                Expanded(
                                  child: Container(
                                    child: FlatButton(
                                      onPressed: rightButtonAction,
                                      child: Text(
                                        rightButtonText,
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      color: Colors.black.withOpacity(0.1),
                                      padding: EdgeInsets.all(15),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
              ],
            )
          ],
        ),
      ),
    );
  }
}


descriptionWidget(
    {title1,
    title2,
    IconData icon,
    Color IconColor,
    Color imageColor,
    Color textColor,
    ImageProvider image,
    bool haveImage}) {
  return Expanded(
    child: Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          haveImage
              ? Container()
              : Icon(
                  icon,
                  color: IconColor,
                ),
          haveImage
              ? Image(
                  image: image,
                  color: imageColor,
                  height: 35,
                  width: 35,
                )
              : Container(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(bottom: 5),
                    child: Text(
                      title1,
                      style: TextStyle(color: textColor),
                    )),
                Text(
                  title2,
                  style: TextStyle(color: textColor),
                ),
              ],
            ),
          )
        ],
      ),
    ),
  );
}

sunInfoRow({title, String imageString, time}) {
  return Row(
    children: <Widget>[
      Expanded(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 10),
          child: Row(
            children: <Widget>[
              Image.asset(
                imageString,
                width: 35,
                height: 35,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18),
                child: Text(title, style: TextStyle(color: Colors.white),),
              ),
            ],
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18),
        child: Text(time, style: TextStyle(color: Colors.white),),
      ),
    ],
  );
}

 fiveDaysForcast({imageString, DateTime date, humidity, mintemp, maxtemp }) {
  return Padding(
    padding: const EdgeInsets.only(top:15.0),
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal:5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(DateFormat("E\n hh:mm  ").format(date),textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
          Container(
//      color: Colors.red,
            margin: EdgeInsets.only(top: 10),
            height: 70,
            width: 70,
            child: Stack(
              children: <Widget>[
                Positioned(
                    top: 0,
                    left: 0,
                    child: Image(image: NetworkImage(imageString,scale: 2.5),),),
                Center(
                  child: Transform.rotate(
                    angle: -math.pi / 4,
                    child: Divider(
                      color: Colors.black,
                      height: 40,
                      thickness: 1,
                      indent: 1,
                      endIndent: 1,
                    ),
                  ),
                ),
                Positioned(
                    bottom: 10,
                    right: 10,
                    child: Icon(
                      Icons.opacity,color: Colors.blue,
                    )),
              ],
            ),
          ),
          Text("$humidity % ", style: TextStyle(color: Colors.white),),
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            Text("$mintemp\u00B0/$maxtemp\u00B0",style: TextStyle(color: Colors.white),),
          ],)

        ],
      ),
    ),
  );
}

class PathExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: PathPainter(),
    );
  }
}

class PathPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = Colors.red
      ..style = PaintingStyle.stroke
      ..strokeWidth = 8.0;

    Path path = Path();
    // TODO: do operations here
    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
