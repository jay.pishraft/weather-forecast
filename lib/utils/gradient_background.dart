import 'package:flutter/material.dart';

class PageGradientBackground extends BoxDecoration {
  @override
  // TODO: implement gradient
  Gradient get gradient {
    return LinearGradient(
//        colors: [Color(0xff0486C8), Color(0xff1C4A71)],
        colors: [Colors.blue[900], Colors.blue[100]],
        begin: AlignmentDirectional.topCenter,
        end: AlignmentDirectional.bottomCenter);
  }

}

class PageTransparentBackGround extends BoxDecoration {
  @override
  // TODO: implement gradient
  Gradient get gradient {
    return LinearGradient(
        colors: [Colors.transparent, Colors.transparent],
        begin: AlignmentDirectional.topCenter,
        end: AlignmentDirectional.bottomCenter);
  }

}
