import 'package:weather_flutter/ModelLayer/model/weather.dart';




import 'constant.dart';
import 'network_util.dart';

class RestDatasource {
  APIRequest _netUtil = new APIRequest();


  static final weatherBaseUrl = kBaseAPIURL;




  Future<WeatherResponse> getWeather(Coordinate coordinate) async {
    String url= weatherBaseUrl + "weather?lat=${coordinate.lat}&lon=${coordinate.lon}&appid=dac02c0148f893f9c8612467aeaa5022&units=metric";
    return _netUtil.get(url).then((dynamic res) {
      if (res["cod"] == 200) {

        WeatherResponse weatherResponse = WeatherResponse.fromJsonMap(res);

        weatherResponse.coordinate = coordinate;

        return weatherResponse;
      } else {
        final error = res['error'];
        return Future.error(error["message"]);
      }
    });
  }


  Future<WeatherForecast> weatherForecast(Coordinate coordinate) async {
    String url= weatherBaseUrl + "forecast?lat=${coordinate.lat}&lon=${coordinate.lon}&appid=dac02c0148f893f9c8612467aeaa5022&units=metric";
    return _netUtil.get(url).then((dynamic res) {
      if (res["cod"] == "200") {
         return WeatherForecast.fromJsonMap(res);
      } else {
        final error = res['error'];
        return Future.error(error["message"]);
      }
    });
  }

}
