import 'package:shared_preferences/shared_preferences.dart';

class UtilSharedPreference{

  static int gps;

  static void saveStringToSharedPreferences({String key, String value}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (key != null) {
      var oldValue = prefs.getString(key);
      if (oldValue != null) {
        prefs.remove(key);
      }
      print("value : $value");
      await prefs.setString(key, '$value');
      //await and save
    }
  }


  static Future<String> getStringFromSharedPreferences(String key )async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String value = prefs.getString(key);
    if (value == null) {
      print("value is $value");
      return null;
    } else {
      print("value is $value");
      return value;
    }
  }

  static void removeItemFromSharedPreferences(String key )async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var value = prefs.getString(key);
    if (value != null) {
      prefs.remove(key);
    }
  }

  static void clearSharedPreferences()async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();

  }




}