import 'package:get_it/get_it.dart';
import 'package:weather_flutter/ModelLayer/view_models/weather_view_model.dart';
import 'package:weather_flutter/helper/rest_ds.dart';

import 'network_check.dart';

GetIt locator = GetIt();

void setupLocator() {
  //register services
  locator.registerLazySingleton<RestDatasource>(() => RestDatasource());

  locator.registerFactory<NetworkCheck>(() => NetworkCheck());

  locator.registerFactory<WeatherModelView>(() => WeatherModelView());

}
