import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;


class APIRequest {
  static APIRequest _instance = new APIRequest.internal();

  APIRequest.internal();

  factory APIRequest() {
    return _instance;
  }

  final JsonDecoder _decoder = new JsonDecoder();


  Future<dynamic> get(String url) async {

    return http
        .get(url)
        .then((http.Response response) {
      final String res = response.body;

      final int statusCode = response.statusCode;


        if (statusCode < 200 || statusCode > 400 || json == null) {
          return Future.error("Error while fetching data");
        }else{
        return _decoder.convert(res);
      }
    });
  }
}
