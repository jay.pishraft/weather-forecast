import 'package:flutter/material.dart';
import 'package:weather_flutter/screens/HomePage/cities.dart';
import 'package:weather_flutter/screens/HomePage/home_page.dart';



const String homePageRoute = "/home";
const String citiesPageRoute = "/cities";


class Router {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {


      case homePageRoute:
        return MaterialPageRoute(builder: (_) => MainHomePage());

        case citiesPageRoute:
        return MaterialPageRoute(builder: (_) => Cities());


      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                  child: Text('No route defined for ${settings.name}'))
              ,
            )
        );
    }
  }

}
