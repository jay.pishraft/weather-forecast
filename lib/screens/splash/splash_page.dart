import 'package:flutter/material.dart';
import 'package:weather_flutter/helper/util_shared_preferences.dart';
import 'package:weather_flutter/screens/HomePage/home_page.dart';
import '../../utils/gradient_background.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
//  Widget _defaultHome = new MainHomePage();

  AnimationController _controller;
  Animation _animation;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    _controller.repeat(period: Duration(seconds: 3), reverse: true);

      navigateToHomeScreen();

  }



  void navigateToHomeScreen() {
    new Future.delayed(const Duration(seconds: 3), () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => MainHomePage()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: PageGradientBackground(),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  child: ScaleTransition(
                    scale: Tween(begin: 0.5, end: 1.0).animate(CurvedAnimation(
                        parent: _controller, curve: Curves.decelerate)),
                    child: Image.asset(
                      "assets/images/weather03-512.png",
                    ),
                  ),
                  height: 200,
                ),
                ScaleTransition(
                    scale: Tween(begin: 0.5, end: 1.0).animate(CurvedAnimation(
                        parent: _controller, curve: Curves.decelerate)),
                    child: Text(
                      "Weather Forecast",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 28),
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

}
