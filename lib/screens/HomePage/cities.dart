import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:weather_flutter/ModelLayer/model/city.dart';
import 'package:weather_flutter/ModelLayer/model/weather.dart';
import 'package:weather_flutter/utils/gradient_background.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

typedef IntCallback = Function(List<City> updatedList);

class Cities extends StatefulWidget {
  final List<City> passedCities;

  final IntCallback addedCities;

  const Cities({Key key, this.passedCities, this.addedCities})
      : super(key: key);

  @override
  _CitiesState createState() => _CitiesState();
}

class _CitiesState extends State<Cities> {
  static final kInitialPosition = LatLng(3.166667, 101.7);

  List<City> suggestions = [
    City(
        id: 1,
        name: "Kuala Lumpur",
        coordinate: Coordinate(lat: 3.139003, lon: 101.686855),
        country: "Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia"),
    City(
        id: 2,
        name: "George Town",
        coordinate: Coordinate(lat: 5.414130699999999, lon: 100.3287506),
        country: "George Town, Penang, Malaysia"),
    City(
        id: 3,
        name: "Johor Bahru",
        coordinate: Coordinate(lat: 1.492659, lon: 103.7413591),
        country: "Johor Bahru, Johor, Malaysia"),
    City(
        id: 4,
        name: "Malacca",
        coordinate: Coordinate(lat: 2.189594, lon: 102.2500868),
        country: "Malacca, Malaysia "),
    City(
        id: 5,
        name: "Ipoh",
        coordinate: Coordinate(lat: 4.597479, lon: 101.090106),
        country: "Ipoh, Perak, Malaysia"),
    City(
        id: 6,
        name: "Kuching",
        coordinate: Coordinate(lat: 1.553504, lon: 110.3592927),
        country: "Kuching, Sarawak, Malaysia"),
    City(
        id: 7,
        name: "Shah Alam",
        coordinate: Coordinate(lat: 3.073281, lon: 101.518461 ),
        country: "Shah Alam, Selangor, Malaysia"),
    City(
        id: 8,
        name: "Kuantan",
        coordinate: Coordinate(lat: 3.7633859, lon: 103.2201828),
        country: "Kuantan, Pahang, Malaysia"),
    City(
        id: 9,
        name: "Klang",
        coordinate: Coordinate(lat: 3.044917, lon: 101.4455621),
        country: "Klang, Selangor, Malaysia"),
    City(
        id: 10,
        name: "Seremban",
        coordinate: Coordinate(lat: 2.725889, lon: 101.9378239),
        country: "Seremban, Negeri Sembilan, Malaysia"),
  ];

  GlobalKey key = new GlobalKey<AutoCompleteTextFieldState<City>>();

  AutoCompleteTextField<City> textField;

  @override
  void initState() {
    super.initState();
  }

  _CitiesState() {
    textField = new AutoCompleteTextField<City>(
      decoration: new InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.symmetric(horizontal: 5, vertical: 15),
          hintText: "Search City:",
          suffixIcon: new Icon(Icons.search)),
      itemSubmitted: (item) {
        bool isDuplicated = false;

        widget.passedCities.forEach((city) {
          if (city.coordinate.lat == item.coordinate.lat &&
              city.coordinate.lon == item.coordinate.lon) {
            isDuplicated = true;
          }
        });
        if (!isDuplicated) {
          setState(() {
            widget.passedCities.add(item);
          });
        }
      },
      key: key,
      suggestions: suggestions,
      itemBuilder: (context, suggestion) => Card(
        color: Colors.white.withOpacity(0.5),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20))),
        child: new ListTile(
            title: new Text(suggestion.name),
            subtitle: new Text("${suggestion.country}")),
      ),
      itemSorter: (a, b) => 0,
      itemFilter: (suggestion, input) =>
          suggestion.name.toLowerCase().startsWith(input.toLowerCase()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return new Future(() {
          Navigator.of(context).pop();
          widget.addedCities(widget.passedCities);

          return false;
        });
      },
      child: Container(
          decoration: PageGradientBackground(),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              title: Text("Locations List"),
              centerTitle: true,
              backgroundColor: Colors.transparent,
              elevation: 0,
              actions: <Widget>[
                FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    focusColor: Colors.transparent,
                    splashColor: Colors.blue.shade100,
                    color: Colors.transparent,
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return PlacePicker(
                              apiKey: 'AIzaSyBxZhfWmc-4uAhj2xVGbu733hrY6Tmgvyg',
                              initialPosition: kInitialPosition,
                              useCurrentLocation: false,
                              //usePlaceDetailSearch: true,
                              onPlacePicked: (result) {

                                bool isDuplicated = false;

                                widget.passedCities.forEach((city) {
                                  if (city.coordinate.lat == result.geometry.location.lat &&
                                      city.coordinate.lon == result.geometry.location.lng) {
                                    isDuplicated = true;
                                  }
                                });
                                if (!isDuplicated) {
                                  City newCity = City(
                                      id: 1,
                                      name: result.name,
                                      coordinate: Coordinate(
                                          lat: result.geometry.location.lat,
                                          lon: result.geometry.location.lng),
                                      country: result.formattedAddress);

                                  widget.passedCities.add(newCity);

                                }

                                Navigator.of(context).pop();
                                setState(() {});
                              },
                              forceSearchOnZoomChanged: true,
                              automaticallyImplyAppBarLeading: false,
                              autocompleteTypes: ['(cities)'],
                              usePinPointingSearch: false,
                              // autocompleteLanguage: "ko",
                              //region: 'my',
                              // selectInitialPosition: true,
                              // selectedPlaceWidgetBuilder: (_, selectedPlace, state, isSearchBarFocused) {
                              //   print("state: $state, isSearchBarFocused: $isSearchBarFocused");
                              //   return isSearchBarFocused
                              //       ? Container()
                              //       : FloatingCard(
                              //           bottomPosition: 0.0,    // MediaQuery.of(context) will cause rebuild. See MediaQuery document for the information.
                              //           leftPosition: 0.0,
                              //           rightPosition: 0.0,
                              //           width: 500,
                              //           borderRadius: BorderRadius.circular(12.0),
                              //           child: state == SearchingState.Searching
                              //               ? Center(child: CircularProgressIndicator())
                              //               : RaisedButton(
                              //                   child: Text("Pick Here"),
                              //                   onPressed: () {
                              //                     // IMPORTANT: You MUST manage selectedPlace data yourself as using this build will not invoke onPlacePicker as
                              //                     //            this will override default 'Select here' Button.
                              //                     print("do something with [selectedPlace] data");
                              //                     Navigator.of(context).pop();
                              //                   },
                              //                 ),
                              //         );
                              // },
                              // pinBuilder: (context, state) {
                              //   if (state == PinState.Idle) {
                              //     return Icon(Icons.favorite_border);
                              //   } else {
                              //     return Icon(Icons.favorite);
                              //   }
                              // },
                            );
                          },
                        ),
                      );
                    },
                    child: Text(
                      "Map",
                      style: TextStyle(color: Colors.white),
                    ))
              ],
            ),
            body: new ListView(
//                  physics: NeverScrollableScrollPhysics(),
                children: [
                  new Padding(
                      child: new Container(
                        child: textField,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                      ),
                      padding: EdgeInsets.all(16.0)),
                  ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: widget.passedCities.length,
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (context, index) {


                        if([0,1,2,3].contains(index)){
                          return Container(
                            child: Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new ListTile(
                                    title:
                                    Padding(
                                      padding: const EdgeInsets.only(bottom:8.0),
                                      child: new Text(widget.passedCities[index].name),
                                    ),
                                    subtitle: new Text(
                                        "${widget.passedCities[index].country}")),
                              ),
                            ),
                          );
                        }else{
                          return Dismissible(

                            key: ObjectKey(widget.passedCities[index]),
                            child: Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new ListTile(
                                    title:
                                    Padding(
                                      padding: const EdgeInsets.only(bottom:8.0),
                                      child: new Text(widget.passedCities[index].name),
                                    ),
                                    subtitle: new Text(
                                        "${widget.passedCities[index].country}")),
                              ),
                            ),
                            onDismissed: (direction) {


                              var item = widget.passedCities.elementAt(index);
                              //To delete
                              deleteItem(index);

                              //To show a snackbar with the UNDO button
                              Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text("Item deleted"),
                                  action: SnackBarAction(
                                      label: "UNDO",
                                      onPressed: () {
                                        //To undo deletion
                                        undoDeletion(index, item);
                                      })));
                            },
                          );
                        }




                      }),
                ]),
          )),
    );
  }

  void deleteItem(index) {
    /*
  By implementing this method, it ensures that upon being dismissed from our widget tree,
  the item is removed from our list of items and our list is updated, hence
  preventing the "Dismissed widget still in widget tree error" when we reload.
  */

      setState(() {
        widget.passedCities.removeAt(index);
      });


  }

  void undoDeletion(index, item) {
    /*
  This method accepts the parameters index and item and re-inserts the {item} at
  index {index}
  */
    setState(() {
      widget.passedCities.insert(index, item);
    });
  }
}
