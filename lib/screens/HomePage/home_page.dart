import 'dart:io';

import 'package:basic_utils/basic_utils.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:weather_flutter/ModelLayer/enums/view_states.dart';
import 'package:weather_flutter/ModelLayer/model/city.dart';
import 'package:weather_flutter/ModelLayer/model/weather.dart';
import 'package:weather_flutter/ModelLayer/view_models/base_view.dart';
import 'package:weather_flutter/ModelLayer/view_models/weather_view_model.dart';
import 'package:weather_flutter/helper/util_shared_preferences.dart';
import 'package:weather_flutter/screens/HomePage/cities.dart';
import 'package:weather_flutter/utils/gradient_background.dart';
import 'package:weather_flutter/widgets/dialogs.dart';
import 'package:flutter/rendering.dart';

class MainHomePage extends StatefulWidget {
  @override
  State createState() => new MainHomePageState();
}

class MainHomePageState extends State<MainHomePage>
    with TickerProviderStateMixin {
  List<City> _cities = [];

  Future<Position> _getLocation() async {
    return await Geolocator().getCurrentPosition(
      desiredAccuracy: LocationAccuracy.lowest,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: PageGradientBackground(),
      child: BaseView<WeatherModelView>(
          onModelReady: (model) async {
            String cities =
                await UtilSharedPreference.getStringFromSharedPreferences(
                    "cities");

            if (cities == null) {
              _cities = [
                City(
                    id: 1,
                    name: "Kuala Lumpur",
                    coordinate: Coordinate(lat: 3.139003, lon: 101.686855),
                    country:
                        "Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia"),
                City(
                    id: 2,
                    name: "George Town",
                    coordinate:
                        Coordinate(lat: 5.414130699999999, lon: 100.3287506),
                    country: "George Town, Penang, Malaysia"),
                City(
                    id: 3,
                    name: "Johor Bahru",
                    coordinate: Coordinate(lat: 1.492659, lon: 103.7413591),
                    country: "Johor Bahru, Johor, Malaysia"),
              ];

              bool geolocationStatus =
                  await Geolocator().isLocationServiceEnabled();
              Position currentPosition = Position();
              if (geolocationStatus) {
                try {
                  currentPosition = await _getLocation();
                  GeolocationStatus permissionStatus =
                      await Geolocator().checkGeolocationPermissionStatus();
                  if (permissionStatus == GeolocationStatus.granted) {
                    _cities.insert(
                        0,
                        City(
                            id: 1,
                            name: "Current Position",
                            coordinate: Coordinate(
                                lat: currentPosition.latitude,
                                lon: currentPosition.longitude),
                            country: ""));
                  }
                } catch (error) {
                  if (error is PlatformException) {
                    if (Platform.isIOS) {
                      exit(0);
                    } else {
                      SystemChannels.platform
                          .invokeMethod('SystemNavigator.pop');
                    }
                  }
                }
              } else {
                showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (context) => AlertDialogWithDynamicActionButton(
                          isNoButton: false,
                          isTwoButton: false,
                          isError: true,
                          isErrorTitle: true,
                          isTopTitle: true,
                          message: "Please turn on GPS to Continue",
                          rightButtonText: "ok",
                          rightButtonAction: () {
                            if (Platform.isIOS) {
                              exit(0);
                            } else {
                              SystemChannels.platform
                                  .invokeMethod('SystemNavigator.pop');
                            }
                          },
                        ));
                return;
              }
            } else {
              _cities = City.decodeCities(cities);
            }

            await model.getCitiesWeather(cities: _cities);
          },
          builder: (context, child, model) => Scaffold(
                backgroundColor: Colors.transparent,
                appBar: AppBar(
                  title: Text("Location"),
                  centerTitle: true,
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Cities(
                                      passedCities: _cities,
                                      addedCities:
                                          (List<City> addedCities) async {
                                        await model.getCitiesWeather(
                                            cities: addedCities);
                                        setState(() {});
                                      },
                                    )));
                      },
                      icon: Icon(
                        Icons.add,
                        size: 30,
                      ),
                    )
                  ],
                ),
                body: _pageCarousel(model: model),
              )),
    );
  }

  _pageCarousel({WeatherModelView model}) {
    switch (model.state) {
      case ViewState.Retrieved:
        return _pageCarouselBody(model: model);
        break;

      case ViewState.Busy:
        return Stack(
          children: <Widget>[
            Center(
                child: CircularProgressIndicator(
              backgroundColor: Colors.white,
            ))
          ],
        );
        break;

      case ViewState.Error:
        return _handleError(model: model);
        break;
      default:
        return _pageCarouselBody(model: model);
    }
  }

  _handleError({WeatherModelView model}) {
    Future(() {
      showDialog(
          context: context,
          builder: (context) => AlertDialogWithDynamicActionButton(
                isNoButton: false,
                isTwoButton: false,
                isError: true,
                isErrorTitle: true,
                isTopTitle: true,
                message: model.error,
                rightButtonText: "ok",
                rightButtonAction: () {
                  if (Platform.isIOS) {
                    exit(0);
                  } else {
                    SystemChannels.platform
                        .invokeMethod('SystemNavigator.pop');
                  }
                },
              ));
    });
    return _pageCarouselBody(model: model);
  }

  Widget _pageCarouselBody({WeatherModelView model}) {
    return Container(
      child: CarouselSlider(
        options: CarouselOptions(
          enableInfiniteScroll: false,
          viewportFraction: 1,
          height: MediaQuery.of(context).size.height,
          enlargeCenterPage: true,
          onPageChanged: (index, reason) {},
        ),
        items: model.citiesWeather.map((weatherResponse) {
          return Builder(
            builder: (BuildContext context) {
              return RefreshIndicator(
                onRefresh: () async {
                  if (model.citiesWeather.indexOf(weatherResponse) == 0) {
                    bool geolocationStatus =
                        await new Geolocator().isLocationServiceEnabled();

                    if (geolocationStatus) {
                      Position currentPosition = await _getLocation();

                      weatherResponse.coordinate.lat = currentPosition.latitude;
                      weatherResponse.coordinate.lon =
                          currentPosition.longitude;
                    } else {
//                        model.setState(ViewState.Error);
                      showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (context) =>
                              AlertDialogWithDynamicActionButton(
                                isNoButton: false,
                                isTwoButton: false,
                                isError: true,
                                isErrorTitle: true,
                                isTopTitle: true,
                                message: "Please turn on GPS to Continue",
                                rightButtonText: "ok",
                                rightButtonAction: () {
                                  Navigator.pop(context);
                                },
                              ));
                      return;
                    }

                    await model.refreshCityWeather(
                        coordinate: weatherResponse.coordinate, isCurrentLocation: true);
                  }else{
                    await model.refreshCityWeather(
                        coordinate: weatherResponse.coordinate, isCurrentLocation: false);

                  }
                  setState(() {});

                },
                child: ListView(
                  children: <Widget>[
                    _cityName(
                        currentLocation:
                            model.citiesWeather.indexOf(weatherResponse) == 0
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    child: Text(
                                      "Current Position",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                  )
                                : Container(),
                        weather: weatherResponse),
                    _date(weatherResponse),
                    _temperature(weatherResponse),
                    _realFeelingAndDescription(weatherResponse),
                    _humidityAndWindSpeed(weatherResponse),
                    _sunInfo(weatherResponse),
                    Card(
                      color: Colors.black12,
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Container(
                        height: 180,
                        width: MediaQuery.of(context).size.width,
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          child: ListView.builder(
                              itemCount: weatherResponse
                                  .weatherForecast.weathers.length,
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                return fiveDaysForcast(
                                  imageString: weatherResponse.weatherForecast
                                      .weathers[index].weather.icon,
                                  date: weatherResponse
                                      .weatherForecast.weathers[index].dateTime,
                                  humidity: weatherResponse.weatherForecast
                                      .weathers[index].main.humidity
                                      .round(),
                                  mintemp: weatherResponse.weatherForecast
                                      .weathers[index].main.tempMin
                                      .round(),
                                  maxtemp: weatherResponse.weatherForecast
                                      .weathers[index].main.tempMax
                                      .round(),
                                );
                              }),
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          );
        }).toList(),
      ),
    );
  }

  _cityName({Widget currentLocation, WeatherResponse weather}) {
    return Padding(
      padding: const EdgeInsets.only(top: 17),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.location_on,
                      color: Colors.white,
                    ),
                    Text(
                      weather.name,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          currentLocation
        ],
      ),
    );
  }

  _date(WeatherResponse weatherResponse) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(
          DateFormat("E, dd-MM-yyyy hh:mm a").format(weatherResponse.dateTime),
          style: TextStyle(color: Colors.white.withOpacity(0.8), fontSize: 18),
        ),
      ),
    );
  }

  _temperature(WeatherResponse weatherResponse) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image(
          image: NetworkImage(weatherResponse.weather.icon, scale: 1),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 13.0, bottom: 5),
          child: Text(
            "${weatherResponse.main.temp.round()}\u00B0",
            style: TextStyle(fontSize: 70, color: Colors.white),
          ),
        )
      ],
    );
  }

  _realFeelingAndDescription(WeatherResponse weatherResponse) {
    return Column(
      children: <Widget>[
        Text(
            "${weatherResponse.main.tempMin.round()}\u00B0/${weatherResponse.main.tempMax.round()}\u00B0 Feels Like ${weatherResponse.main.feelsLike.round()}\u00B0",
            style:
                TextStyle(fontSize: 20, color: Colors.white.withOpacity(0.8))),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
              "${StringUtils.capitalize(weatherResponse.weather.description)}",
              style: TextStyle(
                  fontSize: 20, color: Colors.white.withOpacity(0.8))),
        ),
      ],
    );
  }

  _humidityAndWindSpeed(WeatherResponse weatherResponse) {
    return Card(
      elevation: 0,
      color: Colors.black12,
      margin: EdgeInsets.only(top: 30, bottom: 10, left: 5, right: 5),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: IntrinsicHeight(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 30),
          child: Row(
            children: <Widget>[
              descriptionWidget(
                  haveImage: false,
                  icon: Icons.opacity,
                  IconColor: Colors.blue.shade300,
                  textColor: Colors.white,
                  title1: "Humidity",
                  title2: "${weatherResponse.main.humidity.round()}%"),
              VerticalDivider(
                color: Colors.black54,
                thickness: 0.5,
              ),
              descriptionWidget(
                  haveImage: true,
                  imageColor: Colors.red,
                  image: AssetImage("assets/images/windIcon.png"),
                  textColor: Colors.white,
                  title1: "Wind Speed",
                  title2: "${weatherResponse.wind.speed}"),
            ],
          ),
        ),
      ),
    );
  }

  _sunInfo(WeatherResponse weatherResponse) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      elevation: 0,
      margin: EdgeInsets.only(bottom: 5, left: 5, right: 5),
      color: Colors.black12,
      child: Column(
        children: <Widget>[
          sunInfoRow(
            imageString: "assets/images/sunriseIcon.png",
            time: DateFormat("hh:mm a").format(weatherResponse.sys.sunrise),
            title: "Sunrise",
          ),
          Divider(
            color: Colors.white,
            height: 10,
            thickness: 0.5,
            indent: 20,
            endIndent: 20,
          ),
          sunInfoRow(
            imageString: "assets/images/sunsetIcon.png",
            time: DateFormat("hh:mm a").format(weatherResponse.sys.sunset),
            title: "Sunset",
          ),
        ],
      ),
    );
  }
}
