import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app_config.dart';
import 'helper/service_locator.dart';
import 'main_common.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() async {
  FlavorConfig(
    flavor: Flavor.DEVELOPMENT,
    values: FlavorValues(baseUrl: "https://api.openweathermap.org/data/2.5/"),
  );
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(WeatherApp());
  });



}
