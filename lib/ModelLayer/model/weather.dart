class WeatherResponse {

  String name;
  Coordinate coordinate;
  Wind wind;
  Main main;
  Sys sys;
  DateTime dateTime;
  WeatherForecast weatherForecast;
  Weather weather;


  WeatherResponse(WeatherResponse weather);

  WeatherResponse.fromJsonMap(Map<String, dynamic> json) {

    name = json['name'];
    coordinate = Coordinate.fromJsonMap(json['coord']);
    wind = Wind.fromJsonMap(json['wind']);
    main = Main.fromJsonMap(json['main']);
    sys = Sys.fromJsonMap(json['sys']);
    dateTime = DateTime.fromMillisecondsSinceEpoch(json['dt'] * 1000);
    weather = Weather.fromJsonMap(json['weather'][0]);


  }

}

class Coordinate{
  double lon;
  double lat;

  Coordinate({this.lon,this.lat});

  Coordinate.fromJsonMap(Map<String, dynamic> json) {
    try{
      lon = double.parse(json['lon'].toString());
      lat = double.parse(json['lat'].toString());
    }catch(e){
      print(e.toString());
    }
  }

}

class Weather{
  int id;
  String main;
  String description;
  String icon;

 Weather(Weather description);

 Weather.fromJsonMap(Map<String, dynamic> json) {
    try{
      id = json['id'];
      main = json['main'];
      description = json['description'];
      icon = "http://openweathermap.org/img/wn/${json['icon']}@2x.png";
    }catch(e){
      print(e.toString());
    }
  }

}


class Wind{
  double speed;
  double deg;

  Wind(Wind wind);

  Wind.fromJsonMap(Map<String, dynamic> json) {
    try{
      speed = double.parse(json['speed'].toString());
      deg = double.parse(json['deg'].toString());
    }catch(e){
      print(e.toString());
    }
  }

}


class Main{
  double temp;
  double feelsLike;
  double tempMin;
  double tempMax;
  double pressure;
  double humidity;


  Main(Main main);

  Main.fromJsonMap(Map<String, dynamic> json) {
    try{
      temp= double.parse(json['temp'].toString());
      feelsLike= double.parse(json['feels_like'].toString());
      tempMin= double.parse(json['temp_min'].toString());
      tempMax= double.parse(json['temp_max'].toString());
      humidity= double.parse(json['humidity'].toString());

    }catch(e){
      print(e.toString());
    }
  }

}

class Sys{
  DateTime sunrise;
  DateTime sunset;
  String country;

  Sys(Sys sys);

  Sys.fromJsonMap(Map<String, dynamic> json) {
    try{

      sunrise= DateTime.fromMillisecondsSinceEpoch(json['sunrise'] * 1000);
      sunset= DateTime.fromMillisecondsSinceEpoch(json['sunset'] * 1000);
      country= json['country'];

    }catch(e){
      print(e.toString());
    }
  }



}

class WeatherForecast{

List<WeatherResponse> weathers;


WeatherForecast(WeatherForecast weatherForecast);

WeatherForecast.fromJsonMap(Map<String, dynamic> json) {
  try{
    weathers= json['list'] != null
        ? (json['list'] as List).map((i) {
      return WeatherResponse.fromJsonMap(i);
    }).toList()
        : [];

  }catch(e){
    print(e.toString());
  }
}
}