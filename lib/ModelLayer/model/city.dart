import 'dart:convert';

import 'package:weather_flutter/ModelLayer/model/weather.dart';

class City{
  int id;
  String name;
  String country;
  Coordinate coordinate;

  City({this.id,this.name,this.coordinate,this.country});

  factory City.fromJson(Map<String, dynamic> jsonData) {

    String coordinate = jsonData['coordinate'];
    String lon = coordinate.split(',')[0].split(':')[1];

    String tempLat = coordinate.split(',')[1].split(':')[1];

    String lat=tempLat.substring(0,tempLat.length-1);

    return City(
      id: jsonData['id'],
      name: jsonData['name'],
      country: jsonData['country'],
      coordinate: Coordinate(lon: double.parse(lon),lat: double.parse(lat)),
    );
  }


  static Map<String, dynamic> toMap(City city) => {
    'id': city.id,
    'name': city.name,
    'country': city.country,
    'coordinate': "{lon:${city.coordinate.lon},lat:${city.coordinate.lat}}",
  };

  static String encodeCities(List<City> cities) => json.encode(
    cities
        .map<Map<String, dynamic>>((city) => City.toMap(city))
        .toList(),
  );

  static List<City> decodeCities(String cities) =>
      (json.decode(cities) as List<dynamic>)
          .map<City>((item) => City.fromJson(item))
          .toList();

}