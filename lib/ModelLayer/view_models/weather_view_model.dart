import 'dart:convert';
import 'dart:io';

import 'package:weather_flutter/ModelLayer/enums/view_states.dart';
import 'package:weather_flutter/ModelLayer/model/city.dart';
import 'package:weather_flutter/ModelLayer/model/weather.dart';
import 'package:weather_flutter/helper/util_shared_preferences.dart';

import '../../helper/network_check.dart';
import '../../helper/rest_ds.dart';
import '../../helper/service_locator.dart';
import 'base_model.dart';

class WeatherModelView extends BaseModel {
  RestDatasource api = locator<RestDatasource>();

  NetworkCheck networkCheck = locator<NetworkCheck>();

  List<City> _addedCities=[];
  List<City> get added =>_addedCities;

  List<WeatherResponse> _citiesWeather=[];
  List<WeatherResponse> get citiesWeather => _citiesWeather;

//  Weather _weather;
//  Weather get weather => _weather;


  dynamic _error;
  dynamic get error => _error;


  reSet() {
    setState(ViewState.Retrieved);
  }


//  Future getWeather({City city}) async {
//    setState(ViewState.Busy);
//
//    try {
//      final internet = await networkCheck.check();
//      if (internet != null && internet) {
//
//        _weather =  await api.getWeather(city.coordinate);
//       //print("Im here ${_leave.leaveTypeName}");
//        setState(ViewState.Retrieved);
//
//      } else {
//        _error = 'no internet';
//        setState(ViewState.Error);
//      }
//    } catch (error) {
//      if (error is SocketException) {
//        _error = 'no internet';
//        setState(ViewState.Error);
//      } else {
//        _error = error.toString();
//        setState(ViewState.Error);
//      }
//    }
//  }

  Future getCitiesWeather({List<City> cities}) async {
    setState(ViewState.Busy);

    try {

      final internet = await networkCheck.check();
      if (internet != null && internet)  {





        for(var i=0;i<cities.length;i++){


//          _citiesWeather.any((test)=> test.coordinate==cities[i].coordinate)
          if(!_citiesWeather.any((test)=> test.coordinate==cities[i].coordinate)){
            WeatherResponse weather =  await api.getWeather(cities[i].coordinate);

            WeatherForecast weatherForecast=await api.weatherForecast(cities[i].coordinate);
            weather.weatherForecast=weatherForecast;
            if(i != 0){
              weather.name =  cities[i].name;
            }

            _citiesWeather.add(weather);

          }

        }

        _addedCities=cities;

          _citiesWeather.removeWhere((item){

            bool isDeleted=true;
            _addedCities.forEach((f){
              if(item.coordinate==f.coordinate){
                isDeleted= false;
                return;
              }

            });
           if(isDeleted){

             return true;
           }else{
             return false;
           }

          });

        final String encodedData = City.encodeCities(cities);

        UtilSharedPreference.removeItemFromSharedPreferences("cities");

        UtilSharedPreference.saveStringToSharedPreferences(key: "cities",value: encodedData);



//         cities.forEach((city) async{
//          WeatherResponse weather =  await api.getWeather(city.coordinate);
//          WeatherForecast weatherForecast=await api.weatherForecast(city.coordinate);
//          weather.weatherForecast=weatherForecast;
//          _citiesWeather.add(weather);
//           setState(ViewState.Retrieved);
//        });

            setState(ViewState.Retrieved);


      } else {
        _error = 'no internet';
        setState(ViewState.Error);

      }
    } catch (error) {
      if (error is SocketException) {
        _error = 'no internet';
        setState(ViewState.Error);
      } else {
        _error = error.toString();
        setState(ViewState.Error);
      }
    }
  }



  Future refreshCityWeather({Coordinate coordinate, bool isCurrentLocation}) async {
//    setState(ViewState.Busy);

    try {

      final internet = await networkCheck.check();
      if (internet != null && internet)  {


        WeatherResponse weatherResponse =  await api.getWeather(coordinate);

        WeatherForecast weatherForecast = await api.weatherForecast(coordinate);
        weatherResponse.weatherForecast = weatherForecast;


//        weatherResponse.name="Jay";
         if(!isCurrentLocation){
           weatherResponse.name = _citiesWeather[_citiesWeather.indexWhere((item) => item.coordinate == coordinate)].name;
         }

        _citiesWeather[_citiesWeather.indexWhere((item) => item.coordinate == coordinate)] = weatherResponse;

        _addedCities[0].coordinate=coordinate;

        final String encodedData = City.encodeCities(_addedCities);

        UtilSharedPreference.removeItemFromSharedPreferences("cities");

        UtilSharedPreference.saveStringToSharedPreferences(key: "cities",value: encodedData);

//        setState(ViewState.Retrieved);


      } else {
        _error = 'no internet';
        setState(ViewState.Error);
      }
    } catch (error) {
      if (error is SocketException) {
        _error = 'no internet';
        setState(ViewState.Error);
      } else {
        _error = error.toString();
        setState(ViewState.Error);
      }
    }
  }



}

